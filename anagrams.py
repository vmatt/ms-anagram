# Write a function that, for a given input string, returns all
# anagrams found in the words file.
#
# Requirements:
# 1. Efficient implementation of get_anagrams(). Please explain in the comments the computational complexity of your solution.
# 2. Additional unit test coverage (with unittest or other framework).

import unittest
from collections import defaultdict

class Anagrams:
    def __init__(self, words_file_path):
        # init class overall O(n), later get_anagram usage average is O(1)
        with open(words_file_path) as f:
            # lowercase before splitting, as anagrams are case-insensitive
            words = f.read().lower().splitlines()
            # words has duplicate records, but we'll dedupe in the next step, better to do it here than in the loop below
            # O(2n) for tuple(set()) operation
            words = tuple(set(words))
            # using defaultdict factory to avoid boilerplate checking
            self.word_map = defaultdict(list)

            # O(n) + O(x log x) + O(x) + O(1)  (looping + sorting + tuple conversion exception, append)
            for word in words:
                # using tuple is more memory efficient than list, and it's hashable
                # O(x log x) for sorted, l < 50
                # O(x) for tuple conversion, l < 50
                # O(1) for append
                self.word_map[tuple(sorted(word))].append(word)


    def get_anagrams(self, word):
        # average O(1) as 'w' and 'y' can be expected to < 50

        # worst case O(w log w) for sorting, worst case O(w) for tuple conversion, w < 50
        word_chars = tuple(sorted(word))
        # O(1) for get
        anagrams = self.word_map.get(word_chars, [])

        # seems expected output is in alphabetical order, must be sorted to pass the test
        # worst case O(y log y) for sorting where a is the number of anagrams, y < 20
        return sorted(anagrams)




class TestAnagrams(unittest.TestCase):
    def setUp(self):
        self.words_file_path = "words.txt"

    def test_word_file(self):
        # does the file have at least 2 words?
        with open(self.words_file_path) as f:
            rows = f.read().splitlines()
            self.assertGreater(len(rows), 1)

    def test_anagrams(self):
        anagrams = Anagrams(self.words_file_path)
        # in assertEqual, first param is expected, 2nd is the actual value
        # original setup / assertEqual(actual, expected) will result in misleading error messages!
        self.assertEqual(['dictionary', 'indicatory'], anagrams.get_anagrams('dictionary'))


    def test_id_most_anagrammable_word(self):
        anagrams = Anagrams(self.words_file_path)

        len_max_anagram = 0
        anagrams_of_max_anagrams_word = []
        for i, words in enumerate(anagrams.word_map.values()):
            len_current_anagrams = len(words)
            if len_current_anagrams > len_max_anagram:
                len_max_anagram = len_current_anagrams
                anagrams_of_max_anagrams_word = words

        print("Most anagrammable word count: ", len_max_anagram, anagrams_of_max_anagrams_word)
        self.assertGreater(len(anagrams_of_max_anagrams_word), 0)
